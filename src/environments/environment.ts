// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCBD5xPTd28xPU1_LO9H2F0lzpzN0Ew3To",
    authDomain: "test-d4fed.firebaseapp.com",
    projectId: "test-d4fed",
    storageBucket: "test-d4fed.appspot.com",
    messagingSenderId: "468641730578",
    appId: "1:468641730578:web:2f7b57b83fd316f36288cb"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
