import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class StudentService {

  
  constructor(private db:AngularFirestore) { }

//נגדיר משתנים
studentsCollection:AngularFirestoreCollection;
userCollection:AngularFirestoreCollection = this.db.collection(`users`);

public getStudents(userId){ 
  this.studentsCollection = this.db.collection(`users/${userId}/students`)
  return this.studentsCollection.snapshotChanges().pipe(map(
    collection => collection.map(
      document => {
        const data = document.payload.doc.data(); 
        data.id = document.payload.doc.id; 
        return data; 
      }
    )
  ))
}



deleteStudent(Userid:string,id:string){ 
  this.db.doc(`users/${Userid}/students/${id}`).delete();
}





addStudent(userId:string, name:string, math:number, psychometric:number, payOrNot:boolean){ 
  const student = {name:name, math:math, psychometric:psychometric, payOrNot:payOrNot}; 
  this.userCollection.doc(userId).collection('students').add(student); 
}




updateStudent(userId:string, id:string, name:string, math:number, psychometric:number, payOrNot:boolean){ 
  this.db.doc(`users/${userId}/students/${id}`).update( 
    { 
      name:name, 
      math:math,
      psychometric:psychometric,
      payOrNot:payOrNot
    }
  )
}




  public savePredict(userId, id, fallOrNot){
    this.db.doc(`users/${userId}/students/${id}`).update(
      {
        fallOrNot:fallOrNot
      }
    )
  }




}
