import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ApiService {


  constructor(private http:HttpClient) { }


//נגדיר משתנים
  private url = "https://tnzmbad8pj.execute-api.us-east-1.amazonaws.com/beta";



  predict(psychometric:number, math:number, payOrNot:boolean):Observable<any>{
    let json = {
      "data": 
        {
          "psychometric": psychometric,
          "math": math,
          "payOrNot": payOrNot
        }
    }
    let body  = JSON.stringify(json);
    console.log("in predict");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log("in the map"); 
        console.log(res);
        console.log(res.body);
        return res.body;       
      })
    );      
  }





}
