import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Student } from '../student';

@Component({
  selector: 'studentform',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {

  constructor() { }

  //נגדיר משתנים
  error;
  
  @Input() id: string;
  @Input() name: string;
  @Input() math: number;
  @Input() psychometric: number;
  @Input() payOrNot: boolean;
  @Input() formType: string;
  
  @Output() update = new EventEmitter<Student>(); 
  @Output() closeEdit = new EventEmitter<null>();

  
  
  
  
  ngOnInit(): void {
  }

  updateParent(){
    let student:Student = {id:this.id, name:this.name, math:this.math, psychometric:this.psychometric, payOrNot:this.payOrNot}; 
    this.update.emit(student); 
    if (this.formType == "Add Student"){ 
      this.name = null;
      this.math = null;
      this.psychometric = null;
    }
  }


  tellParenttoClose(){
    this.closeEdit.emit(); 
  }

  validation(math,psychometric){
    if (math < '100' && math > '0'){
      this.error = ''
      this.updateParent()
    }
    else{
      this.error = 'Please choose between the math 0-100'
    }
    if (psychometric < '800' && psychometric > '0'){
      this.error = ''
      this.updateParent()
    }
    else{
      this.error = 'Please choose between the psychometric 0-800'
    }
  }


  

}
