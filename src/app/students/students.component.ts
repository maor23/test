import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { AuthService } from '../auth.service';
import { Student } from '../student';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})

export class StudentsComponent implements OnInit {

  
  constructor(private apiService:ApiService, public authService:AuthService, private studentService:StudentService) { }

  
    
  //נגדיר משתנים
    math:number;
    psychometric:number;
    category:string;
    prob;
    payOrNot;
    fallOrNot;
    panelOpenState = false;  
    students$; 
    userId:string;
    editstate = [];  
    addstudentFormOpen = false; 
  
  
  
  
  ngOnInit(): void {
    this.authService.getUser().subscribe( 
      user =>{
        this.userId = user.uid; 
        console.log(this.userId)
        this.students$ = this.studentService.getStudents(this.userId); 
    }
      )
  }





  deleteStudent(id:string){ 
  this.studentService.deleteStudent(this.userId,id); 
}




add(student:Student){
  this.studentService.addStudent(this.userId, student.name, student.math, student.psychometric, student.payOrNot);
}




update(student:Student){
  this.studentService.updateStudent(this.userId, student.id, student.name, student.math, student.psychometric, student.payOrNot);
}




predict(psychometric, math, payOrNot){
    this.apiService.predict(psychometric, math, payOrNot).subscribe(
      res => { 
        console.log(res); 
        this.prob = res;
      }
    )
    if(this.prob >= 0.5){
      this.fallOrNot = "yes";
    }
    else{
      this.fallOrNot = "no";
    }
  }


  savePredict(id, fallOrNot){
    this.studentService.savePredict(this.userId,id,fallOrNot);
  }



}


