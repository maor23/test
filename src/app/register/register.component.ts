import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  
  constructor(private authService:AuthService, private router:Router) { }

  
  //נגדיר משתנים
    hide = true; 
    email:string;
    password:string;
    errorMessage:string; 
    isError:boolean = false; 
  
  
  
    onSubmit(){ 
      this.authService.SignUp(this.email, this.password)
      .then(res => {   
            console.log(res); 
            this.router.navigate(['/welcome']) 
          })
        .catch(err => { 
            console.log(err); 
            this.isError = true; 
            this.errorMessage = err.message; 
          }) 
      }

  
  
  
    ngOnInit(): void {
  }



}
