export interface Student {
    id: string,
    name: string,
    math: number,
    psychometric: number,
    payOrNot: boolean, 
}
